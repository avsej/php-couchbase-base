FROM centos:7

MAINTAINER Sergey Avseyev <sergey@couchbase.com>

RUN yum upgrade -y && yum install -y iproute https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm https://rpms.remirepo.net/enterprise/remi-release-7.rpm && yum install -y php71-php-cli php71-php-mbstring php71-php-pecl-psr php71-php-pecl-couchbase2 libcouchbase-tools && yum clean all -y && rm -rf /var/cache/yum/*

COPY php-entrypoint /usr/local/bin/
COPY 99-local.ini /etc/opt/remi/php71/php.d/

ENTRYPOINT ["php-entrypoint"]
CMD ["php71", "-a"]
